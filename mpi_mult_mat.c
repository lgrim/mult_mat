#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"
#define MASTER	0

int  len, rc, numtasks, taskid;
char hostname[MPI_MAX_PROCESSOR_NAME];
MPI_Status status;

int main (int argc, char **argv)
{
  unsigned long int lines, columns, linhas_por_tarefa, qtd_posicoes;
  register unsigned long int i, j, k;
  long int *matA = NULL;
  long int *localMatA = NULL;
  long int *matB = NULL;
  long int *matResult = NULL;
  long int *localMatResult = NULL;
  long int somatorio;

  /***** Inicializações do MPI *****/ 
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  MPI_Get_processor_name(hostname, &len);

  // Verifica os parâmetros de entrada
  switch(argc)
  {
      case 0:
      case 1:
      case 2:
          fprintf(stderr, "Uso:\n\t%s <nº de linhas> <nº de colunas> \n", argv[0]);
	  MPI_Abort(MPI_COMM_WORLD, rc);
          exit(EXIT_FAILURE);
          break;
      case 3:
          lines = atoi(argv[1]);
          columns = atoi(argv[2]);
  }

  /*Aborta a execução se o programa for inicializado com menos de 2 processos)*/
  if (numtasks<2)
  {
	printf("\n\nExecucao abortada, o programa deve ser executado com pelo menos 2 processos\n");
	MPI_Abort(MPI_COMM_WORLD, rc);
   	exit(0);
  }

  /*Aborta a execução se o nº de linhas não for divisível pelo nº de tarefas*/
  if (lines % numtasks != 0)
  {
  	printf("\n\nExecucao abortada, o numero de linhas deve ser divisivel pelo numero de tarefas\n");
	MPI_Abort(MPI_COMM_WORLD, rc);
   	exit(0);
  }

  /* Define tamanhos parciais e quantidade de posições */
  linhas_por_tarefa = lines / numtasks;
  qtd_posicoes = linhas_por_tarefa * columns;

  // Alocação dinâmica das matrizes
  /* Todas as tarefas alocam as matrizes A e Result parciais*/
  localMatA = (long int *) malloc(sizeof(long int) * linhas_por_tarefa * columns);
  localMatResult = (long int *) malloc(sizeof(long int) * linhas_por_tarefa * columns);
  
  /* Matriz B é alocada cheia em todas as tarefas */
  matB = (long int *) malloc(sizeof(long int) * lines * columns);

  // Verifica se alocou as matrizes parciais A e Result e Matriz B cheia
  if ((localMatA == NULL)||(localMatResult == NULL)||(matB == NULL))
  {
      perror("Estourou a memória!\n");
      MPI_Abort(MPI_COMM_WORLD, rc);
      exit(EXIT_FAILURE);
      return -1;
  }
  
  /* Tarefa MASTER aloca Matriz A e Result e gera as matrizes A e B cheias */
  if(taskid == MASTER)
  {
	  matA = (long int *) malloc(sizeof(long int) * lines * columns);
	  matResult = (long int *) malloc(sizeof(long int) * lines * columns);

	  // Verifica se alocou as matrizes A e Result
	  if ((matA == NULL)||(matResult == NULL))
	  {
	      perror("Estourou a memória!\n");
              MPI_Abort(MPI_COMM_WORLD, rc);
	      exit(EXIT_FAILURE);
	      return -1;
	  }

	  // Gera matrizes A e B com números aleatórios
	  srand( time(NULL) );
	  for (i=0; i<lines; i++)
	  {
	      for(j=0; j<columns; j++)
	      {
		 matA[(i*columns) + j] = rand() % (lines*columns);
		 matB[(i*columns) + j] = rand() % (lines*columns);
	      }
	  }
  }

  /*O MASTER divide a Matriz A e passa as parciais aos respectivos processos */
  MPI_Scatter(matA, qtd_posicoes, MPI_LONG, localMatA, qtd_posicoes, MPI_LONG, MASTER, MPI_COMM_WORLD);
  
  /*A matriz B deve ser passada inteira por broadcast por conta do processo de multiplicaçao */
  MPI_Bcast(matB, lines*columns, MPI_LONG, 0, MPI_COMM_WORLD);
  
  // Mutiplica A x B e guarda em MatResult.
  for (i=0; i<linhas_por_tarefa; i++)
  {
 	 for (j=0; j<columns; j++)
 	 {
	        somatorio=0;
		for (k=0; k<columns; k++)
		{
			somatorio += localMatA[(i*columns) + k] * matB[(k*columns) + j];
		}
		localMatResult[(i*columns) + j] = somatorio;
	  }
     	  printf("Tarefa %d em %s calculando na linha %ld\n", taskid, hostname,linhas_por_tarefa*taskid+i);
  } 

  /*MASTER coleta as localMatResult e junta em MatResult na tarefa MASTER*/
  MPI_Gather(localMatResult, qtd_posicoes, MPI_LONG, matResult, qtd_posicoes, MPI_LONG, MASTER, MPI_COMM_WORLD);
  
  // Desaloca as matrizes
  free(localMatA);
  free(matB);
  free(localMatResult);
  if(taskid == MASTER)
  {
  	free(matA);
  	free(matResult);
  }
  
  /* Finaliza a comunicação MPI */
  MPI_Finalize();
  return 0;
}
