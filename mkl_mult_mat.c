#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mkl.h"

int NUM_THREADS;

int main (int argc, char **argv)
{
  unsigned lapack_int lines, columns;
  register unsigned lapack_int i, j, k;
  double *matA;
  double *matB;
  double *matResult;
  double somatorio;

  // Verifica os parâmetros de entrada
  switch(argc)
  {
      case 0:
      case 1:
      case 2:
      case 3:
          fprintf(stderr, "Uso:\n\t%s <nº de linhas> <nº de colunas> <nº de threads>\n", argv[0]);
          exit(EXIT_FAILURE);
          break;
      case 4:
          lines = atoi(argv[1]);
          columns = atoi(argv[2]);
	  NUM_THREADS = atoi(argv[3]);
  }

  // Alocação dinâmica das matrizes
  matA = (double *) malloc(sizeof(double) * lines * columns);
  matB = (double *) malloc(sizeof(double) * lines * columns);
  matResult = (double *) malloc(sizeof(double) * lines * columns);

  // Verifica se alocou todas as matrizes
  if ((matA == NULL)||(matB == NULL)||(matResult == NULL))
  {
      perror("Estourou a memória!\n");
      exit(EXIT_FAILURE);
      return -1;
  }

  // Gera matrizes A e B com números aleatórios
  srand( time(NULL) );
  for (i=0; i<lines; i++)
  {
      for(j=0; j<columns; j++)
      {
         matA[(i*columns) + j] = rand() % (lines*columns);
	 matB[(i*columns) + j] = rand() % (lines*columns);
      }
  }

  // Configura o nº de threads a ser utilizado;
  mkl_set_num_threads(NUM_THREADS);
  
  // Mutiplica A x B e guarda em MatResult.
  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, lines, columns, columns, 1.0, matA, columns, matB, columns, 0.0, matResult, columns);
	 
  // Desaloca as matrizes
  free(matA);
  free(matB);
  free(matResult);

  return 0;
}
